package az.ingress.lessons.demo.service;

import az.ingress.lessons.demo.domain.CourseEntity;
import az.ingress.lessons.demo.domain.StudentEntity;
import az.ingress.lessons.demo.dto.StudentDto;
import az.ingress.lessons.demo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImp implements StudentService {

    private final ModelMapper modelMapper;
    private final StudentRepository studentRepository;


    @Override
    public List<StudentDto> listStudent() {
        return studentRepository.findAll()
                .stream()
                .map(studentEntity -> modelMapper.map(studentEntity , StudentDto.class))
                .toList();
    }

}
