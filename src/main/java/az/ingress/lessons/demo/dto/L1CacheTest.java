package az.ingress.lessons.demo.dto;

import az.ingress.lessons.demo.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class L1CacheTest {

    private final StudentRepository studentRepository;

    public void loadData(){
        studentRepository.findById(1L);

    }
}
