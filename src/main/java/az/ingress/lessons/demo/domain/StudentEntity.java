package az.ingress.lessons.demo.domain;


import jakarta.persistence.*;
import lombok.*;

import java.util.List;


@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "students")

public class StudentEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;

    //,fetch = FetchType.EAGER
    //, cascade = CascadeType.PERSIST
    @OneToMany(mappedBy = CourseEntity.Fields.students ,cascade = CascadeType.PERSIST)
    private List<CourseEntity> courses;



}
