package az.ingress.lessons.demo.repository;

import az.ingress.lessons.demo.domain.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<StudentEntity,Long> {

}
