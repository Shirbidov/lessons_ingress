package az.ingress.lessons.demo;

import az.ingress.lessons.demo.domain.CourseEntity;
import az.ingress.lessons.demo.domain.StudentEntity;
import az.ingress.lessons.demo.repository.StudentRepository;
import az.ingress.lessons.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;
@RequiredArgsConstructor
@SpringBootApplication

public class DemoApplication implements CommandLineRunner {

	private final StudentService studentService;
	private final StudentRepository studentRepository;


	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}


	@Override
	public void run(String... args) throws Exception {

		for (int i = 0; i < 100; i++) {
		studentRepository.save(studentEntity());
		}

	}

	private StudentEntity studentEntity(){
		StudentEntity studentEntity =
				StudentEntity
						.builder()
						.name("Ruslan")
						.surname("Shirbidov")
						.build();

		CourseEntity courseEntity1  =
				CourseEntity
						.builder()
						.students(studentEntity)
						.name("MS 24")
						.build();
		CourseEntity courseEntity2  =
				CourseEntity
						.builder()
						.students(studentEntity)
						.name("OCA")
						.build();
		studentEntity.setCourses(List.of(courseEntity1 , courseEntity2));
		return studentEntity;
	}

}
